package com.example.dell.demofacebook.Class;

/**
 * Created by DELL on 22/10/2016.
 */

public class UserFriend {
  private String userName;
  private String userId;
  private String Url;
  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUrl() {
    return Url;
  }

  public void setUrl(String url) {
    Url = url;
  }

  public UserFriend(String userName, String userId, String url) {

    this.userName = userName;
    this.userId = userId;
    Url = url;
  }
}

package com.example.dell.demofacebook.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.dell.demofacebook.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;

public class MainActivity extends AppCompatActivity {
  LoginButton loginButton;
  CallbackManager callbackManager;
  AccessToken token;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // init Facebook SDK
    creatFacebookSdk();

    setContentView(R.layout.activity_main);

    // Event
    setSdkFactoryAndLoginButton();

    // check Login
    checkFacebookLogin();

    // Callback registration
    registerFacebookCallBack();
  }

  private void setSdkFactoryAndLoginButton() {
    callbackManager = CallbackManager.Factory.create();
    loginButton = (LoginButton) findViewById(R.id.login_button);
    loginButton.setReadPermissions("user_friends","user_posts");
  }

  private void registerFacebookCallBack() {
    loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
      @Override
      public void onSuccess(LoginResult loginResult) {
        getListFriend();
      }

      @Override
      public void onCancel() {
        // App code
      }

      @Override
      public void onError(FacebookException exception) {
        // App code
      }
    });
  }

  private void creatFacebookSdk() {
    // creat Facebook SDK
    FacebookSdk.sdkInitialize(getApplicationContext());
    AppEventsLogger.activateApp(this);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    callbackManager.onActivityResult(requestCode, resultCode, data);
  }

  private void checkFacebookLogin() {
    token = AccessToken.getCurrentAccessToken();
    if (token != null) {
      getListFriend();
    }
  }

  private void getListFriend() {
    GraphRequest request = GraphRequest.newMyFriendsRequest(
        AccessToken.getCurrentAccessToken(),
        new GraphRequest.GraphJSONArrayCallback() {
          @Override
          public void onCompleted(JSONArray array, GraphResponse response) {
            Intent a = new Intent(MainActivity.this, ListFriendActivity.class);
            try {
              JSONArray arr = response.getJSONObject().getJSONArray("data");
              a.putExtra("jsonData", arr.toString());
              startActivity(a);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        });
    request.executeAsync();
  }
}

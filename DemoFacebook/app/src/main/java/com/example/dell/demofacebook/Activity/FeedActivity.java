package com.example.dell.demofacebook.Activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.dell.demofacebook.Adapter.UserFeedAdapter;
import com.example.dell.demofacebook.Class.UserFeed;
import com.example.dell.demofacebook.R;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class FeedActivity extends AppCompatActivity implements View.OnClickListener {
  ListView mUserFeedListView;
  UserFeedAdapter mUserFeedAdapter;
  ArrayList<UserFeed> mUserFeedArrayList = new ArrayList<UserFeed>();
  ArrayList<UserFeed> mUserFeedArrayListLoadMore = new ArrayList<>();
  JSONArray jsonArray = null;
  int maxItem = 8;
  SwipeRefreshLayout layout;
  Button btnLoadMore;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_feed);

    // init array user feed
    getUserFeed();

    // creat adapter and set listview
    initAdapterAndListview();

    // create button load more
    setButtonLoadMore();

//    // set event pull to refresh
    initRefeshLayout();
  }

  private void initAdapterAndListview() {
    mUserFeedListView = (ListView) findViewById(R.id.lvUserFeed);
    mUserFeedAdapter = new UserFeedAdapter(getApplicationContext(), R.layout
        .layout_listview_userfeed_row, mUserFeedArrayList);
    mUserFeedListView.setAdapter(mUserFeedAdapter);
  }

  private void initRefeshLayout() {
    layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
    layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        getUserFeed();
        layout.setRefreshing(false);
      }
    });
    layout.setProgressViewOffset(false,0,300);
    layout.setEnabled(true);
  }

  private void getUserFeed() {
    if (mUserFeedArrayList.size() > 0) {
      mUserFeedAdapter.clear();
      mUserFeedArrayListLoadMore.clear();
    }

    // get and add data to array
    GraphRequest request = GraphRequest.newGraphPathRequest(
        AccessToken.getCurrentAccessToken(),
        "/me/feed",
        new GraphRequest.Callback() {
          @Override
          public void onCompleted(GraphResponse response) {
            // Insert your code here
            try {
              jsonArray = response.getJSONObject().getJSONArray("data");
            } catch (JSONException e) {
              e.printStackTrace();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
              String userStory = null, userMessage = "", userTime = "";
              try {
                userStory = jsonArray.getJSONObject(i).getString("story");
              } catch (JSONException e) {
                e.printStackTrace();
              }
              try {
                userMessage = jsonArray.getJSONObject(i).getString("message");
              } catch (JSONException e) {
                e.printStackTrace();
              }
              try {
                userTime = jsonArray.getJSONObject(i).getString("created_time");
              } catch (JSONException e) {
                e.printStackTrace();
              }

              mUserFeedArrayListLoadMore.add(new UserFeed(userMessage, userStory, userTime));
            }
            // add item to listView
            for (int i = 0; i < maxItem; i++) {
              mUserFeedArrayList.add(mUserFeedArrayListLoadMore.get(i));
            }
            mUserFeedAdapter.notifyDataSetChanged();
          }
        });
    request.executeAsync();
  }

  private void setButtonLoadMore() {

    btnLoadMore = new Button(this);
    btnLoadMore.setText("Load More");
    mUserFeedListView.addFooterView(btnLoadMore);

    btnLoadMore.setOnClickListener(this);

  }

  @Override
  public void onClick(View v) {
    for (int i = maxItem; i < maxItem + 8; i++) {
      if (i == mUserFeedArrayListLoadMore.size()) {
        break;
      }
      mUserFeedArrayList.add(mUserFeedArrayListLoadMore.get(i));
      mUserFeedListView.setSelection(maxItem);
    }
    maxItem += 8;
    if (maxItem > mUserFeedArrayListLoadMore.size())
      maxItem = mUserFeedArrayListLoadMore.size();
    mUserFeedAdapter.notifyDataSetChanged();
  }
}

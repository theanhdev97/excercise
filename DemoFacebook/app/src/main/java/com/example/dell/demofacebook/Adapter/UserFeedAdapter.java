package com.example.dell.demofacebook.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dell.demofacebook.Class.UserFeed;

import java.util.ArrayList;

import static com.example.dell.demofacebook.R.id.tvUserFeedRow;

/**
 * Created by DELL on 24/10/2016.
 */

public class UserFeedAdapter extends ArrayAdapter<UserFeed> {
  private Context context;
  private int resource;
  private ArrayList<UserFeed> arr;

  public class ViewHolder {
    TextView tvUserFeed;

    public ViewHolder(View view) {
      tvUserFeed = (TextView) view.findViewById(tvUserFeedRow);
    }
  }

  public UserFeedAdapter(Context context, int resource, ArrayList<UserFeed> objects) {
    super(context, resource, objects);
    this.context = context;
    this.resource = resource;
    this.arr = objects;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View a = convertView;

    UserFeedAdapter.ViewHolder viewHolder;
    if (a == null) {
      LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      a = layoutInflater.inflate(resource, parent, false);
      viewHolder = new UserFeedAdapter.ViewHolder(a);
      a.setTag(viewHolder);
    }
    viewHolder = (UserFeedAdapter.ViewHolder) a.getTag();

    UserFeed userFeed = arr.get(position);
    if (userFeed != null) {
      viewHolder.tvUserFeed.setText("Message : " + userFeed.getUserMessage());
      viewHolder.tvUserFeed.setText(viewHolder.tvUserFeed.getText() + "\nCreated time : " + userFeed
          .getUserCreatedTime());
      viewHolder.tvUserFeed.setText(viewHolder.tvUserFeed.getText() + "\n" + "Story : " + userFeed
          .getUserStory());
    }
    return a;
  }
}

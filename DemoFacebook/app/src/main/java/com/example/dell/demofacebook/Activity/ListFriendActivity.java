package com.example.dell.demofacebook.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.dell.demofacebook.Adapter.UserFriendAdapter;
import com.example.dell.demofacebook.AsyncTask.FriendListAsync;
import com.example.dell.demofacebook.Class.UserFriend;
import com.example.dell.demofacebook.R;
import com.facebook.GraphRequest;

import java.util.ArrayList;

public class ListFriendActivity extends AppCompatActivity {
  public static ArrayList<UserFriend> userFriendArray ;
  public static UserFriendAdapter userFriendAdapter ;
  public static ListView userFriendListView ;

  GraphRequest graphRequest;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_friend);

    // find Id
    findViewById();

    // init variable
    initVariable();

    // add adapter and set Adapter
    new FriendListAsync(this, getIntent().getStringExtra("jsonData")).execute();
  }
  private void initVariable() {
    userFriendArray = new ArrayList<UserFriend>();
    userFriendAdapter = new UserFriendAdapter(this, R.layout.layout_listview_userfriend_row, userFriendArray);
    userFriendListView.setAdapter(userFriendAdapter);
    userFriendListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent a = new Intent(ListFriendActivity.this, FeedActivity.class);
        startActivity(a);
      }
    });
  }

  private void findViewById(){
    userFriendListView = (ListView) findViewById(R.id.listView);
  }
}

package com.example.dell.demofacebook.AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.example.dell.demofacebook.Activity.ListFriendActivity;
import com.example.dell.demofacebook.Class.UserFriend;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by DELL on 24/10/2016.
 */

public class FriendListAsync extends AsyncTask<Void, Void, Void> {
  Activity mActivity;
  String jsondata;
  ArrayList<String> userIdArray = new ArrayList<String>();
  ArrayList<String> userNameArray = new ArrayList<String>();

  public FriendListAsync(Activity activity, String jsondata) {
    this.mActivity = activity;
    this.jsondata = jsondata;
  }

  @Override
  protected Void doInBackground(Void... params) {

    // get json friend array
    GraphRequest request = GraphRequest.newMyFriendsRequest(
        AccessToken.getCurrentAccessToken(),
        new GraphRequest.GraphJSONArrayCallback() {
          @Override
          public void onCompleted(final JSONArray array, GraphResponse response) {
            // add to array
            try {
              JSONArray friendslist = new JSONArray(jsondata);
              Log.d("friendlist", friendslist.toString());
              for (int i = 0; i < friendslist.length(); i++) {
                String userName = friendslist.getJSONObject(i).get("name").toString();
                String userId = friendslist.getJSONObject(i).get("id").toString();
                userNameArray.add(userName);
                userIdArray.add(userId);
              }

              for (int i = 0; i < userIdArray.size(); i++) {
                // get User Avatar
                Bundle params = new Bundle();
                params.putBoolean("redirect", false);

                final int finalI = i;
                new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    userIdArray.get(i) + "/picture",
                    params,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                      public void onCompleted(GraphResponse response) {
                        try {
                          String userName = userNameArray.get(finalI);
                          String userId = userIdArray.get(finalI);
                          // get user avatar url
                          String userAvatar = (String) response.getJSONObject().getJSONObject("data").get
                              ("url");

                          // add UserFriend to adapter
                          ListFriendActivity.userFriendArray.add(new UserFriend(userName, userId,
                              userAvatar));
                          ListFriendActivity.userFriendAdapter.notifyDataSetChanged();
                          Log.d("tag123", ListFriendActivity.userFriendArray.get(0).getUserName() + " : " +
                              "items");
                        } catch (JSONException e) {
                          e.printStackTrace();
                        }
                      }
                    }
                ).executeAsync();
              }

            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        });
    request.executeAsync();
    return null;
  }
}
package com.example.dell.demofacebook.Class;

/**
 * Created by DELL on 23/10/2016.
 */

public class UserFeed {
  private String userMessage;
  private String userStory;
  private String userCreatedTime;


  public String getUserMessage() {

    return userMessage;
  }

  public void setUserMessage(String userMessage) {
    this.userMessage = userMessage;
  }

  public String getUserStory() {
    return userStory;
  }

  public void setUserStory(String userStory) {
    this.userStory = userStory;
  }

  public String getUserCreatedTime() {
    return userCreatedTime;
  }

  public void setUserCreatedTime(String userCreatedTime) {
    this.userCreatedTime = userCreatedTime;
  }

  public UserFeed(String userMessage, String userStory, String userCreatedTime) {
    this.userMessage = userMessage;
    this.userStory = userStory;
    this.userCreatedTime = userCreatedTime;
  }
}

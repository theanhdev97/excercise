package com.example.dell.demofacebook.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dell.demofacebook.Class.UserFriend;
import com.example.dell.demofacebook.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DELL on 22/10/2016.
 */

public class UserFriendAdapter extends ArrayAdapter<UserFriend> {
  private Context context;
  private int resource;
  private ArrayList<UserFriend> arr;

  public class ViewHolder {
    ImageView ivUserIcon;
    TextView tvUserName;

    public ViewHolder(View view) {
      ivUserIcon = (ImageView) view.findViewById(R.id.ivIcon);
      tvUserName = (TextView) view.findViewById(R.id.tvUserName);
    }
  }

  public UserFriendAdapter(Context context, int resource, ArrayList<UserFriend> objects) {
    super(context, resource, objects);
    this.context = context;
    this.resource = resource;
    this.arr = objects;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View a = convertView;

    ViewHolder viewHolder;
    if (a == null) {
      LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      a = layoutInflater.inflate(resource, parent, false);
      viewHolder = new ViewHolder(a);
      a.setTag(viewHolder);
    }
    viewHolder = (ViewHolder) a.getTag();

    UserFriend userFriend = arr.get(position);
    if (userFriend != null) {
      viewHolder.tvUserName.setText(userFriend.getUserName());
      Picasso.with(context)
          .load(userFriend.getUrl())
          .into(viewHolder.ivUserIcon);
    }
    return a;
  }
}
